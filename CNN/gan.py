import keras
from keras.layers import Input
from keras.models import Model

from discriminator import create_discriminator
from generator import create_generator

d = create_discriminator()
g = create_generator()

def create_gan(discriminator, generator):
  discriminator.trainable = False
  gan_input = Input(shape=(100,))
  x = generator(gan_input)
  gan_output= discriminator(x)
  gan = Model(inputs=gan_input, outputs=gan_output)
  gan.compile(loss='binary_crossentropy', optimizer='adam')
  return gan

gan = create_gan(d, g)
gan.summary()