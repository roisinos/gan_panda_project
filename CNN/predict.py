import numpy as np
from keras.models import load_model
from PIL import Image
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

dim = int(28)

def predict_images(number_images, model_src, src):
  model = load_model(model_src)
  noise = np.random.uniform(-1, 1, size=[number_images, 100])
  generated_images = model.predict(noise)
  generated_images = 127.5+generated_images*127.5
  generated_images = generated_images.reshape(number_images,dim,dim)
  for i in range(number_images):
    fig = plt.figure()
    canvas = FigureCanvas(fig)
    ax = fig.gca()
    ax.imshow(generated_images[i], cmap='gray', vmin=0, vmax=255, interpolation='nearest')
    ax.axis('off')
    canvas.draw()
    s, (width, height) = canvas.print_to_buffer()
    im = Image.frombytes("RGBA", (width, height), s)
    im = im.convert('L')
    im = im.crop((143,58,513,428))
    im = im.resize((28,28))
    im = im.convert('RGB')
    im.save(src+'panda-'+str(i)+'.jpg')
    plt.close('all')
    #plt.savefig(src+'panda-'+str(i)+'.jpg')
    del fig, canvas, ax, s, width, height, im
  print('Predicted images generated')
    
predict_images(10, 'panda.h5', 'predicted-images/')