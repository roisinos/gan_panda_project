import keras
from keras.models import Sequential
from keras.layers import Dense, Activation, Flatten
from keras.layers import Conv2D
from keras.layers import LeakyReLU, Dropout
from keras.optimizers import RMSprop

def optimizer():
  optimizer = RMSprop(lr=0.0002, decay=6e-8)
  return optimizer

def create_discriminator():
  img_rows = 28
  img_cols = 28
  channel = 1
  discriminator = Sequential()
  depth = 64
  dropout = 0.4
  # In: 64 x 64 x 1, depth = 1
  # Out: 14 x 14 x 1, depth=64
  input_shape = (img_rows, img_cols, channel)
  discriminator.add(Conv2D(depth * 1, 5, strides=2, input_shape=input_shape, \
                    padding='same'))
  discriminator.add(LeakyReLU(alpha=0.2))
  discriminator.add(Dropout(dropout))

  discriminator.add(Conv2D(depth * 2, 5, strides=2, padding='same'))
  discriminator.add(LeakyReLU(alpha=0.2))
  discriminator.add(Dropout(dropout))

  discriminator.add(Conv2D(depth * 4, 5, strides=2, padding='same'))
  discriminator.add(LeakyReLU(alpha=0.2))
  discriminator.add(Dropout(dropout))

  discriminator.add(Conv2D(depth * 8, 5, strides=1, padding='same'))
  discriminator.add(LeakyReLU(alpha=0.2))
  discriminator.add(Dropout(dropout))

  # Out: 1-dim probability
  discriminator.add(Flatten())
  discriminator.add(Dense(1))
  discriminator.add(Activation('sigmoid'))
  discriminator.compile(loss='binary_crossentropy', optimizer=optimizer(), \
           metrics=['accuracy'])
  return discriminator

d = create_discriminator()
d.summary()