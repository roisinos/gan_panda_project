import keras
from keras.models import Sequential
from keras.layers import Dense, Activation, Reshape
from keras.layers import Conv2DTranspose, UpSampling2D
from keras.layers import Dropout
from keras.layers import BatchNormalization
from keras.optimizers import RMSprop

def optimizer():
  return RMSprop(lr=0.0001, decay=3e-8)


def create_generator():
  generator = Sequential()
  dropout = 0.4
  depth = 64 + 64 + 64 + 64
  dim = 7
  # In: 100
  # Out: dim x dim x depth
  generator.add(Dense(dim * dim * depth, input_dim=100))
  generator.add(BatchNormalization(momentum=0.9))
  generator.add(Activation('relu'))
  generator.add(Reshape((dim, dim, depth)))
  generator.add(Dropout(dropout))

  # In: dim x dim x depth
  # Out: 2*dim x 2*dim x depth/2
  generator.add(UpSampling2D())
  generator.add(Conv2DTranspose(int(depth / 2), 5, padding='same'))
  generator.add(BatchNormalization(momentum=0.9))
  generator.add(Activation('relu'))

  generator.add(UpSampling2D())
  generator.add(Conv2DTranspose(int(depth / 4), 5, padding='same'))
  generator.add(BatchNormalization(momentum=0.9))
  generator.add(Activation('relu'))

  generator.add(Conv2DTranspose(int(depth / 8), 5, padding='same'))
  generator.add(BatchNormalization(momentum=0.9))
  generator.add(Activation('relu'))

  # Out: 28 x 28 x 1 grayscale image [0.0,1.0] per pix
  generator.add(Conv2DTranspose(1, 5, padding='same'))
  generator.add(Activation('sigmoid'))
  generator.compile(loss='binary_crossentropy', optimizer=optimizer(), \
          metrics=['accuracy'])
  return generator


g = create_generator()
g.summary()