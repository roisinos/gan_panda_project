import keras
from keras.layers import Dense, Dropout
from keras.models import Sequential
from keras.layers.advanced_activations import LeakyReLU
from keras.optimizers import Adam

def adam_optimizer():
  return Adam(lr=0.0002, beta_1=0.5)

def create_discriminator():
  discriminator=Sequential()
  discriminator.add(Dense(units=1024, input_dim=784))
  discriminator.add(LeakyReLU(0.2))
  discriminator.add(Dropout(0.3))
  
  discriminator.add(Dense(units=512))
  discriminator.add(LeakyReLU(0.2))
  discriminator.add(Dropout(0.3))
  
  discriminator.add(Dense(units=256))
  discriminator.add(LeakyReLU(0.2))
  
  discriminator.add(Dense(units=1, activation='sigmoid'))
  
  discriminator.compile(loss='binary_crossentropy', optimizer=adam_optimizer())
  return discriminator

d = create_discriminator()
d.summary()