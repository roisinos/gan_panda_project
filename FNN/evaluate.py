import numpy as np
from predict import predict_images
from PIL import Image
import os
import sys
sys.path.append("..")
from TTUR import fid

def gen_real_images(number_images, src):
  data = np.load('../panda.npy')
  images = data[np.random.randint(low=0, high=data.shape[0], size=number_images)]
  for i in range(number_images):
    img = Image.fromarray(images[i], 'L')
    img = img.convert('RGB')
    img.save(src+'panda-'+str(i)+'.jpg')
  print('Real images generated')
#gen_real_images(10, 'real-images/')

def evaluate_images(number_images):
  src_real = 'real-images/'
  src_predicted = 'predicted-images/'
  gen_real_images(number_images, src_real)
  predict_images(number_images, 'panda20.h5', src_predicted)
  fid.main(['real-images/', 'predicted-images/'])
  os.system('rm real-images/*')
  os.system('rm predicted-images/*')

#use python3 evaluate.py src_real src_predicted
evaluate_images(100)