import keras
from keras.layers import Dense
from keras.models import Sequential
from keras.layers.advanced_activations import LeakyReLU
from keras.optimizers import Adam

def adam_optimizer():
  return Adam(lr=0.0002, beta_1=0.5)

def create_generator():
  generator = Sequential()
  generator.add(Dense(units=256, input_dim=100))
  generator.add(LeakyReLU(0.2))
  
  generator.add(Dense(units=512))
  generator.add(LeakyReLU(0.2))
  
  generator.add(Dense(units=1024))
  generator.add(LeakyReLU(0.2))
  
  generator.add(Dense(units=784, activation='tanh'))
  
  generator.compile(loss='binary_crossentropy', optimizer=adam_optimizer())
  return generator

g = create_generator()
g.summary()