import numpy as np
import matplotlib.pyplot as plt
import keras
from tqdm import tqdm

from discriminator import create_discriminator
from generator import create_generator
from gan import create_gan

n_data = 10000
dim = int(28)
dim2 = int(dim*dim)

def load_data():
  x_train = np.load('../panda.npy')
  x_train = x_train[0:n_data]
  x_train = (x_train.astype(np.float32) - 127.5)/127.5
  # convert shape of x_train from (n_data, dim, dim) to (n_data, dim2) 
  # dim2 columns per row
  x_train = x_train.reshape(n_data, dim2)
  return x_train

X_train = load_data()
print(X_train.shape)

def plot_generated_images(epoch, generator, examples=100, dim_im=(10,10), figsize=(10,10)):
  noise = np.random.uniform(-1, 1, size=[examples, 100])
  generated_images = generator.predict(noise)
  generated_images = 127.5+127.5*generated_images
  generated_images = generated_images.reshape(examples,dim,dim)
  plt.figure(figsize=figsize)
  for i in range(generated_images.shape[0]):
    plt.subplot(dim_im[0], dim_im[1], i+1)
    plt.imshow(generated_images[i], cmap='gray', vmin=0, vmax=255, interpolation='nearest')
    plt.axis('off')
  plt.tight_layout()
  plt.savefig('training-images/gan_generated_image %d.png' %epoch)
  plt.close()
    
def training(epochs=1, batch_size=128):
  #Loading the data
  X_train = load_data()
  
  # Creating GAN
  generator = create_generator()
  discriminator = create_discriminator()
  gan = create_gan(discriminator, generator)
  
  for e in range(1,epochs+1 ):
    print("Epoch %d" %e)
    for _ in tqdm(range(batch_size)):
    #generate  random noise as an input  to  initialize the  generator
      noise = np.random.normal(0, 1, [batch_size, 100])
      
      # Generate fake MNIST images from noised input
      generated_images = generator.predict(noise)
      
      # Get a random set of  real images
      image_batch = X_train[np.random.randint(low=0, high=X_train.shape[0], size=batch_size)]
      
      #Construct different batches of  real and fake data 
      X= np.concatenate([image_batch, generated_images])
      
      # Labels for generated and real data
      y_dis = np.zeros(2*batch_size)
      y_dis[:batch_size]=1
      
      #Pre train discriminator on  fake and real data  before starting the gan. 
      discriminator.trainable = True
      discriminator.train_on_batch(X, y_dis)
      
      #Tricking the noised input of the Generator as real data
      noise = np.random.uniform(-1, 1, [batch_size, 100])
      y_gen = np.ones(batch_size)
      
      # During the training of gan, 
      # the weights of discriminator should be fixed. 
      #We can enforce that by setting the trainable flag
      discriminator.trainable = False
      
      #training  the GAN by alternating the training of the Discriminator 
      #and training the chained GAN model with Discriminator’s weights freezed.
      gan.train_on_batch(noise, y_gen)
      
    if e == 1 or e % 20 == 0:
      plot_generated_images(e, generator)
      generator.save('panda'+str(e % 40)+'.h5')
  generator.save('panda.h5')

training(2000,256)